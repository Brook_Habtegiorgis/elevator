
Author: Brook Habtegiorgis
Assignment : GFX Code Review


How to Run the Program:
-------------------------------
The Driver  class is the main class that needs to be run to start this application.

Approach to solve the problem:
--------------------------------------
I have tried to model the problem using  different classes and by assigning a single responsibility for each class . I have used to a strategy pattern to model the elevator with two different modes. I created an Elevator class as context class and Mode interface as strategy. Operation A and Operation B model  the two modes described in the write up and they implement the Mode interface.

I have also used composition to for to separate the process of reading the commands and extracting the numbers from the elevator model. To accomplish that I have created a command class  that models a single command and an InputRead class to read from the file. Elevator class is contains an instance of Input Read.

Implementing Mode A and Mode B 
--------------------------------------------
Implementing Mode A was straightforward as it is only enough to iterate over commands and display the numbers and total floors the elevator traveled. Mode B was more involved. I tried to sort  the commands based on their distance from the initial floor and then check if any of the commands overlap and can be merged to a single command. I was able to show the similar numbers as shown on the sample output but the implementation  still needs some improvement. To accomplish sorting  the  command objects i had to create a CommandComparator class which implements comparator interface and override the compare method two compare command objects.
 
Unit Testing 
---------------
I have included small unit tests and created 3 unit testing classes for the InputRead,OperationA and OperationB classes to test them individually.

