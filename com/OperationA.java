package com;

import java.util.ArrayList;

public class OperationA implements Mode {

	@Override
	public void operate(int[] currentFloor, ArrayList<Command> commands) {

		int totalFloorsTravled = 0;
		System.out.print(currentFloor[0] + " ");

		// iterate through the commands and serve the passengers
		for (Command cmd : commands) {

			// add the distance from current floor to start floor
			totalFloorsTravled += Math.abs(currentFloor[0]
					- cmd.getStartFloor());
			System.out.print(cmd.getStartFloor() + " " + cmd.getEndFloor()
					+ " ");
			// add the floors traveled
			totalFloorsTravled += cmd.getFloorDifference();
			// assign new current floor
			currentFloor[0] = cmd.getEndFloor();
			// command served
			cmd.setCommandServed(true);
		}
		// display the total floors traveled
		System.out.print("(" + totalFloorsTravled + ")  ");
	}
}
