package com;

import java.util.ArrayList;

public class OperationATest {

	public static void main(String[] args) {

		ArrayList<Command> commands = new ArrayList<Command>();

		// Add the test commands
		commands.add(new Command(1, 5));
		commands.add(new Command(1, 6));
		commands.add(new Command(1, 5));

		// starting floor
		int[] currentFloor = { 9 };

		OperationA modeA = new OperationA();
		modeA.operate(currentFloor, commands);
		/*
		 * Expected output : 9 1 5 1 6 1 5 (30)
		 */
	}

}
