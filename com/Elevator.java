package com;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Elevator {

	// object of Mode interface to choose between Mode A and B
	private Mode elevatorMode;
	private InputRead fileReader;

	public void setElevatorMode(Mode selectedMode) {
		this.elevatorMode = selectedMode;
	}

	public void acceptCommandFile(String fileName) {
		try {
			// set the file 
			this.fileReader = new InputRead(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean  readCommands (int [] initialFloor, ArrayList<Command> commands){
	
		// boolean to test if all commands are read
		boolean readStatus; 
		// file reader 
		readStatus = fileReader.readFile(initialFloor, commands);		
		return readStatus;
	}
	
	public void closeFileReader (){
		fileReader.closeScanner();
	}
	
	public void serveCommands (int[] currentFloor,ArrayList <Command> commands)
	{
	    elevatorMode.operate(currentFloor, commands);
	}
	public void resetReader()
	{
		fileReader.reset();
	}
	
}
