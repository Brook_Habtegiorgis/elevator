package com;

public class Command {

	private int startFloor;
	private int endFloor;
	private int floorDifference;
	private boolean commandServed;

	public Command(int startFloor, int endFloor) {
		this.startFloor = startFloor;
		this.endFloor = endFloor;
		floorDifference = Math.abs(endFloor - startFloor);
		setCommandServed(false);
	}

	public int getStartFloor() {
		return startFloor;
	}

	public int getEndFloor() {
		return endFloor;
	}

	public int floordifference() {
		return floorDifference;
	}
    
	public int getFloorDifference() {
		return floorDifference;
	}
	
	public boolean isCommandServed() {
		return commandServed;
	}
    
	public void setCommandServed(boolean commandServed) {
		this.commandServed = commandServed;
	}
}