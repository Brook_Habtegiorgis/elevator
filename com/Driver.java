package com;

import java.util.ArrayList;

public class Driver {

	public static void main(String[] args) {
		
		Elevator elevator = new Elevator();
		String fileName = "test.txt";
		
		// open the command file
		elevator.acceptCommandFile(fileName);
		ArrayList<Command> commands = new ArrayList<Command>();
		
		// initialize one array element to pass by reference 
		int[] initialFloor= {0};
		
		//print modes 
		System.out.println("Mode A");
		System.out.println("------");
		
		
		while (elevator.readCommands(initialFloor, commands)){
	    	// set the elevator to mode A   
			elevator.setElevatorMode(new OperationA());
			// Execute the commands
	        elevator.serveCommands(initialFloor, commands);
	     
			System.out.println();

	        // reset initial floor and commands
	        initialFloor[0]= 0;
	        commands.clear();
		}
		// close the file reader 
		elevator.closeFileReader();
		
		// open another scanner for Mode B
		elevator.acceptCommandFile(fileName);
		
		System.out.println();
		System.out.println("Mode B ");
		System.out.println("------");
		
		while (elevator.readCommands(initialFloor, commands)){
			
	        // set the elevator to mode B   
	        elevator.setElevatorMode(new OperationB());
			// Execute the commands
	        elevator.serveCommands(initialFloor, commands);
			System.out.println();
			
	        // reset initial floor and commands
	        initialFloor[0]= 0;
	        commands.clear();
		}
		// close the file reader 
		elevator.closeFileReader();
	}
}
