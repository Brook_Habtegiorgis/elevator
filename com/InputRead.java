package com;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class InputRead {

	private File inputFile;
	private Scanner scan;

	public InputRead(String inputFileName) throws FileNotFoundException {
		this.inputFile = new File(inputFileName);
		this.scan = new Scanner(inputFile);

	}

	// method to check if the file exists
	public boolean isFileexist() {
		if (inputFile.exists())
			return true;
		else
			return false;
	}

	// @param initialFloor : The initial location of the elevator single element
	// array
	public boolean readFile(int[] initialFloor, ArrayList<Command> commands) {

		// read each command line by line from input file
		if (scan.hasNext()) {
			String line = scan.nextLine();
			// extract only the initial floor and commands
			Pattern commaPattern = Pattern.compile(":|,");
			String[] cmds = commaPattern.split(line);

			// get the initial floor
			initialFloor[0] = Integer.parseInt(cmds[0]);

			// get the commands
			for (int i = 1; i < cmds.length; i++) {
				// extract each floor from every command
				String[] inputs = cmds[i].split("-");

				int startFloor = Integer.parseInt(inputs[0]);
				int endFloor = Integer.parseInt(inputs[1]);

				Command cmd = new Command(startFloor, endFloor);
				commands.add(cmd);

			}
			return true;
		} else {
			return false;
		}
	}

	public void reset() {
		this.scan.reset();
	}

	public void closeScanner() {
		this.scan.close();
	}
}
