package com;

import java.util.ArrayList;

public class OperationBTest {

	public static void main(String[] args) {
		ArrayList<Command> commands = new ArrayList<Command>();

		// Add the test commands
		commands.add(new Command(1, 8));
		commands.add(new Command(6, 8));
		//commands.add(new Command(5, 8));
		//commands.add(new Command(7, 1));
		//commands.add(new Command(11,1));
		
		// starting floor
		int[] currentFloor = {5};

		OperationB modeB = new OperationB();
		modeB.operate(currentFloor, commands);
		
	
		
		/*
		 * Expected output : 3 5 7 8 9 11 1 (18)
		 */ 


	}

}
