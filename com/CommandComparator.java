package com;

import java.util.Comparator;

public class CommandComparator implements Comparator <Command>{

	private int initalFloor;
	public CommandComparator(int initialFloor){
		this.initalFloor = initialFloor;
	}
	
	@Override
	public int compare(Command cmd1, Command cmd2) {
		
		// Compare the distance between initial floor and startFloors
		return (Math.abs(initalFloor - cmd1.getStartFloor())) - (Math.abs(initalFloor - cmd2.getStartFloor())) ;
	}

}
