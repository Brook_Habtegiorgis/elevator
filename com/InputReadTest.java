package com;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class InputReadTest {

	public static void main(String[] args) throws FileNotFoundException {

		// Test if the file is being opened
		String fileName = "test.txt";
		InputRead reader = new InputRead(fileName);
		// check if file exists 
		System.out.println ("File exists : "+ reader.isFileexist());
		
		ArrayList<Command> testCommands = new ArrayList<Command>();
		int [] testInitialFloor = {0};

		// read the file
		reader.readFile(testInitialFloor, testCommands);

		System.out.println("Initial Floor: " + testInitialFloor[0]);
		System.out.println("All Commands:");
		for (Command cmd : testCommands) {
			System.out.println("Start Floor :"+cmd.getStartFloor());
			System.out.println("End Floor:"+cmd.getEndFloor());
			System.out.println("Floor difference:"+ cmd.floordifference());
		}

	}

}
