package com;

import java.util.ArrayList;
import java.util.Collections;

public class OperationB implements Mode {

	@Override
	public void operate(int[] currentFloor, ArrayList<Command> commands) {
		// holds traveled floors
		ArrayList<Integer> visitedFloors = new ArrayList<Integer>();

		int totalFloorsTravled = 0;
		System.out.print(currentFloor[0] + " ");

		// sort based on the distance between initial floor and startFloors
		Collections.sort(commands, new CommandComparator(currentFloor[0]));

		// add the initial floor and the starting floor of the first command
		visitedFloors.add(currentFloor[0]);
		if (currentFloor[0] != commands.get(0).getStartFloor())
				visitedFloors.add(commands.get(0).getStartFloor());

		// serve each command and while simultaneously checking
		// commands that over lap
		for (int i = 0; i < commands.size(); i++) {

			// check if the end floor is overlaps with any start floor from
			// other command
			for (int y = i + 1; y < commands.size(); y++) {
				if (commands.get(i).getEndFloor() > commands.get(y)
						.getStartFloor() && !commands.get(y).isCommandServed()) {
					// found a overlapping command
					visitedFloors.add(commands.get(y).getStartFloor());
				} else if (commands.get(i).getEndFloor() == commands.get(y)
						.getStartFloor()) {
					commands.get(y).setCommandServed(true);
				}
			}
			// add the current end floor after comparison
			visitedFloors.add(commands.get(i).getEndFloor());
		}
		// add the last command 
		if (commands.get(commands.size() -1).isCommandServed()){
			visitedFloors.add(commands.get(commands.size() -1).getEndFloor());
		}
		else {
			visitedFloors.add(commands.get(commands.size()-1).getStartFloor());
			visitedFloors.add(commands.get(commands.size()-1).getEndFloor());
		}
			
		// calculate total floors traveled
		for (int x = 0, z = 1; x < (visitedFloors.size() - 1); x++,z++) {
			totalFloorsTravled += Math.abs(visitedFloors.get(x).intValue()
					- visitedFloors.get(z).intValue());
		}
		// display result
		for (Integer i : visitedFloors) {
			System.out.print(+i + " ");
		}
		System.out.print("(" + totalFloorsTravled + ")  ");
		System.out.println();
	}

}
